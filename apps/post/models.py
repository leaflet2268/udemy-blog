from django.db import models


class Post(models.Model):

    title = models.CharField(max_length=100, verbose_name="タイトル")
    content = models.TextField(verbose_name="本文")

    created_at = models.DateTimeField(auto_now_add=True, verbose_name="作成日")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="最終編集日")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "記事"
        verbose_name_plural = "記事"
