from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from django.contrib import messages

from .models import Post
from .forms import PostForm


class PostMixin:
    model = Post


class PostListView(PostMixin, ListView):
    context_object_name = "blog_posts"
    paginate_by = 3


class PostDetailView(PostMixin, DetailView):
    context_object_name = "post"


class PostCreateView(PostMixin, CreateView):
    success_url = "/"
    form_class = PostForm

    def form_valid(self, form):
        # messages.success(request, 'Profile details updated.')
        messages.success(self.request, "保存しました")
        return super().form_valid(form)


class PostUpdateView(PostMixin, UpdateView):
    form_class = PostForm

    def get_success_url(self):
        # http://127.0.0.1:8000/7/update
        id = self.kwargs.get("pk")  # id = 7

        # path("<int:pk>", PostDetailView.as_view(), name="detail"),
        # http://127.0.0.1:8000/7
        url = reverse_lazy("detail", kwargs={"pk": id})
        return url

    def form_valid(self, form):
        messages.success(self.request, "保存しました")
        return super().form_valid(form)


class PostDeleteView(PostMixin, DeleteView):
    success_url = "/"

    def delete(self, request, *args, **kwargs):
        messages.success(self.request, "削除しました")
        return super().delete(request, *args, **kwargs)
